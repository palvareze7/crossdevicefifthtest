package com.example.fifthtest.Entities;

public class TestMessage {
    private String sender;
    private String base64Image;

    public TestMessage() {
    }

    public TestMessage(String sender, String base64Image) {
        this.sender = sender;
        this.base64Image = base64Image;
    }

    public String getSender() {
        return sender;
    }

    public TestMessage setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public TestMessage setBase64Image(String base64Image) {
        this.base64Image = base64Image;
        return this;
    }
}
