package com.example.fifthtest.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.fifthtest.R;
import android.content.Intent;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class FindServerActivity extends AppCompatActivity {

    EditText editIp;
    Button btnFindServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_server);

        editIp = findViewById(R.id.editIpServer);
        btnFindServer = findViewById(R.id.btnFindServer);

        btnFindServer.setOnClickListener(view -> {
            String ipServer =  editIp.getText().toString();
            if(!ipServer.isEmpty() &&  Patterns.IP_ADDRESS.matcher(ipServer).matches()){
                Intent intent = new Intent(FindServerActivity.this,ClientActivity.class);
                intent.putExtra("ipServer",ipServer);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(),"Ingrese una Ip valida",Toast.LENGTH_SHORT).show();
            }
        });
    }
}

