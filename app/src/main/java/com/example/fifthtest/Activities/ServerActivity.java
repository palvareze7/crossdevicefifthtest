package com.example.fifthtest.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mg.connectionlibraryandroid.Implementations.ConnectMethods;
import com.app.mg.connectionlibraryandroid.Implementations.MessageMethods;
import com.example.fifthtest.Entities.TestMessage;
import com.example.fifthtest.Interfaces.WebSocketReceiver;
import com.example.fifthtest.R;
import com.example.fifthtest.Utilities.ImageUtility;
import com.example.fifthtest.WebSocket.WebSocketClientImp;
import com.example.fifthtest.WebSocket.WebSocketServerImp;
import com.google.gson.Gson;

import org.java_websocket.WebSocket;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerActivity extends AppCompatActivity implements WebSocketReceiver, SensorEventListener2 {

    private  String port = "8080";
    private  String myIpAddress;

    private ConnectMethods connectMethods = new ConnectMethods();

    private WebSocketServerImp wsServer;
    private WebSocketClientImp wsClient;
    private InetSocketAddress inetSockAddress;

    private ImageView imageView;
    private Button btnSubmitImage, btnSendImage, btnCloseSession;
    private TextView myIpTextView;
    private Handler handler;
    MessageMethods<TestMessage ,WebSocketClientImp, WebSocket> messageMethods = new MessageMethods<>();
    private Gson gson = new Gson();
    private boolean connectedByBump = false;

    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private SensorEventListener proximitySensorListener;
    private Date thisBumpDate;
    private Date externalBumpDate;
    private String pairedIpAddress = "";
    private TestMessage messageReceived;

    private final long CHECK_BUMP_TIME = 500;
    private final long DELAY_DETECT_BUMP = 10000;

    private final int MAX_WIDTH_IMAGE = 1400;
    private final int MAX_HEIGHT_IMAGE = 1600;

    private int min_distance = 400;
    private float downX, downY, upX, upY;

    private boolean send_image = true;

    private static final float SHAKE_THRESHOLD = 1.1f;
    private static final int SHAKE_WAIT_TIME_MS = 250;
    private SensorManager mSensorManager;
    private Sensor mSensorAcc;
    private long mShakeTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        myIpAddress = connectMethods.FindMyIpAddress(this);
        btnSendImage = findViewById(R.id.send_data);
        btnSubmitImage = findViewById(R.id.submit_image);
        btnCloseSession = findViewById(R.id.disconnect_server);
        imageView = findViewById(R.id.imageViewServer);
        myIpTextView = findViewById(R.id.text_ip_client);
        myIpTextView.setText(myIpAddress);

        imageView.setOnTouchListener(onTouchListener());

        btnSubmitImage.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");

            startActivityForResult(Intent.createChooser(intent,"Pick Image"), 1);
        });

        btnSendImage.setOnClickListener(view -> {
            sendMessageToServer();
        });

        btnCloseSession.setOnClickListener(view -> {
            CloseServer();
            finish();
        });

        SetWServerAndStart();

        handler = new Handler();
        handler.postDelayed(this::connectWebSocket, 2000);
        initializeProximitySensor();
        thisBumpDate = new Date();
        externalBumpDate = new Date();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    private void initializeProximitySensor() {
        if (proximitySensor == null){
            Toast.makeText(this, "Proximity sensor not available", Toast.LENGTH_SHORT).show();
        }
        else {
            proximitySensorListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    if (sensorEvent.values[0] < proximitySensor.getMaximumRange()){
                        Log.d("PSensor", "Sensor triggered");
                        thisBumpDate = new Date();
                        TestMessage testMessage = new TestMessage()
                                .setSender(myIpAddress)
                                .setBase64Image(new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(thisBumpDate));
                        String test = gson.toJson(testMessage);
                        wsClient.send(test);

                        Log.d("server - external", externalBumpDate.toString());
                        Log.d("server - this", thisBumpDate.toString());
                        Boolean isNotTheSame = Math.abs(externalBumpDate.getTime()-thisBumpDate.getTime()) > CHECK_BUMP_TIME;
                        Boolean isADevice = Math.abs(externalBumpDate.getTime()-thisBumpDate.getTime()) < DELAY_DETECT_BUMP;
                        Log.d("server - this", isNotTheSame.toString());
                        Log.d("server - this", isADevice.toString());

                        if (isNotTheSame && isADevice && (messageReceived != null)){
                            pairedIpAddress = messageReceived.getSender();
                            Log.d("PAIRED", pairedIpAddress);
                            connectedByBump = true;
                        }
                    }
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };
            sensorManager.registerListener(proximitySensorListener, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        sensorManager.unregisterListener(proximitySensorListener);
    }

    @Override
    protected void onResume(){
        super.onResume();
        mSensorManager.registerListener(this, mSensorAcc, SensorManager.SENSOR_DELAY_NORMAL);
        initializeProximitySensor();
    }

    private void connectWebSocket() {
        wsClient = new WebSocketClientImp(connectMethods.GetUriServer(myIpAddress, port), this, this);
        wsClient.connect();
        Toast.makeText(getApplicationContext(),"Cliente Conectado",Toast.LENGTH_SHORT).show();
    }

    private void SetWServerAndStart() {
        inetSockAddress = connectMethods.GetISocketAddres(this, port);
        wsServer = new WebSocketServerImp(inetSockAddress);
        wsServer.setReuseAddr(true);
        wsServer.start();

        Toast.makeText(getApplicationContext(),"Server Abierto",Toast.LENGTH_SHORT).show();
    }

    private void sendMessageToServer() {
        String type = imageView.getDrawable().getClass().getSimpleName();
        if (imageView.getDrawable() != null && type.equals("BitmapDrawable") && send_image){
            Toast.makeText(getApplicationContext(),"Enviando....",Toast.LENGTH_SHORT).show();
            send_image = false;
            btnSendImage.setEnabled(false);
            String base64Image = checkImageResolution(imageView);
            createAndSendMessage(base64Image);
            Toast.makeText(getApplicationContext(),"Imagen Enviada",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),"No hay imagen para enviar",Toast.LENGTH_SHORT).show();
        }
    }

    public void createAndSendMessage(String parseImage){
        TestMessage testMessage = new TestMessage()
                .setSender(myIpAddress)
                .setBase64Image(parseImage);
        String test = gson.toJson(testMessage);
        wsClient.send(test);
    }

    private String checkImageResolution(ImageView imageView){
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        boolean isPortrait = width < height;
        String base64Image;
        if(width <= MAX_WIDTH_IMAGE && height <= MAX_HEIGHT_IMAGE ) {
            base64Image = ImageUtility.convertToBase64(imageView);
        } else {
            base64Image = ImageUtility.resizeImageAndConvertToBase64(imageView, isPortrait);
        }
        return base64Image;
    }

    private void CloseServer(){
        try {
            wsClient.close();
            wsServer.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private OnTouchListener onTouchListener() {
        return (view, motionEvent) -> {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    downX = motionEvent.getX();
                    downY = motionEvent.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    upX = motionEvent.getX();
                    upY = motionEvent.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    //HORIZONTAL SCROLL
                    if(Math.abs(deltaX) > Math.abs(deltaY))
                    {
                        if(Math.abs(deltaX) > min_distance){
                            // left or right
                            if(deltaX < 0)
                            {
                                //Toast.makeText(ServerActivity.this,"right", Toast.LENGTH_SHORT).show();
                                sendMessageToServer();
                                return true;
                            }
                            if(deltaX > 0) {
                                //Toast.makeText(ServerActivity.this,"left", Toast.LENGTH_SHORT).show();
                                sendMessageToServer();
                                return true;
                            }
                        }
                        else {
                            //not long enough swipe...
                            return false;
                        }
                    }
                    //VERTICAL SCROLL
                    else
                    {
                        if(Math.abs(deltaY) > min_distance){
                            // top or down
                            if(deltaY < 0)
                            {
                                //Toast.makeText(ServerActivity.this,"down", Toast.LENGTH_SHORT).show();
                                return true;
                            }
                            if(deltaY > 0)
                            {
                                //Toast.makeText(ServerActivity.this,"top", Toast.LENGTH_SHORT).show();
                                sendMessageToServer();
                                return true;
                            }
                        }
                        else {
                            //not long enough swipe...
                            return false;
                        }
                    }
                    break;
                case MotionEvent.ACTION_MOVE:

                    break;
            }
            return true;
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (wsClient != null){
            CloseServer();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == 1) {
            try{
                InputStream inputStream = getContentResolver().openInputStream(data.getData());

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException ex){

            }
        }
    }

    @Override
    public void onWebSocketMessage(String message) {
        messageReceived = gson.fromJson(message, TestMessage.class);
        if(!messageReceived.getSender().equals(myIpAddress)){
            try{
                externalBumpDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(messageReceived.getBase64Image());
                Log.d("server - datetest", Long.toString(Math.abs(externalBumpDate.getTime()-thisBumpDate.getTime())));
            }catch (ParseException e){
                Log.e("error", e.getMessage());
            }
            Bitmap bitmap = ImageUtility.convertToBitmap(messageReceived.getBase64Image());
            handler.post((Runnable) () -> {
                imageView.setImageBitmap(bitmap);
                btnSendImage.setEnabled(true);
            });
        }
        handler.post((Runnable) () -> {
            btnSendImage.setEnabled(true);
        });
        send_image = true;
    }

    @Override
    public void onWebSocketClose(int code, String reason, boolean remote) {
        if(code == 1001 && remote){
            Toast.makeText(this, "SERVER WAS CLOSE", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (connectedByBump){
            if (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    Toast.makeText(this, "Accelerometer unreliable", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                detectShake(sensorEvent);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    private void detectShake(SensorEvent event) {
        long now = System.currentTimeMillis();

        if ((now - mShakeTime) > SHAKE_WAIT_TIME_MS) {
            mShakeTime = now;

            float gX = event.values[0] / SensorManager.GRAVITY_EARTH;
            float gY = event.values[1] / SensorManager.GRAVITY_EARTH;
            float gZ = event.values[2] / SensorManager.GRAVITY_EARTH;

            // gForce will be close to 1 when there is no movement
            double gForce = Math.sqrt(gX * gX + gY * gY + gZ * gZ);

            // Change background color if gForce exceeds threshold;
            // otherwise, reset the color
            if (gForce > SHAKE_THRESHOLD) {
                connectedByBump = false;
                Toast.makeText(this, "Device unpaired", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

