package com.example.fifthtest.WebSocket;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.fifthtest.Interfaces.WebSocketReceiver;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class WebSocketClientImp extends WebSocketClient {

    private WebSocketReceiver receiverMethod;

    public WebSocketClientImp(URI serverUri) {
        super(serverUri);
    }
    private Context context;

    public WebSocketClientImp(URI serverUri, WebSocketReceiver receiver) {
        super(serverUri);
        this.receiverMethod = receiver;
    }

    public WebSocketClientImp(URI serverUri, WebSocketReceiver receiver, Context context) {
        super(serverUri);
        this.receiverMethod = receiver;
        this.context = context;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        Log.i("CLIENT OPEN","Arrive Open");
    }

    @Override
    public void onMessage(String message) {
        receiverMethod.onWebSocketMessage(message);
        Log.i("CLIENT MESSAGE:","Arrive Message");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        receiverMethod.onWebSocketClose(code,reason,remote);
        Log.i("CLIENT CLOSE:","Arrive Close");
    }

    @Override
    public void onError(Exception ex) {
        Log.i("CLIENT ERROR","Arrive error: "+ex.getMessage());
    }
}