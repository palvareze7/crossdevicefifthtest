package com.example.fifthtest.WebSocket;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class WebSocketServerImp extends WebSocketServer {

    List<String> bumpUriList;

    public  WebSocketServerImp(InetSocketAddress address){
        super(address);
        bumpUriList = new ArrayList<>();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        Log.i("SERVER OPEN","Arrive open");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        conn.close();
        Log.i("SERVER CLOSE","Arrive close");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        broadcast(message);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        Log.i("SERVER ERROR","Arrive Error");
    }

    @Override
    public void onStart() {

    }
}